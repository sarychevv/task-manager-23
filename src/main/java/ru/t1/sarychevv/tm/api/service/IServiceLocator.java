package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.model.IAuthService;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

}
